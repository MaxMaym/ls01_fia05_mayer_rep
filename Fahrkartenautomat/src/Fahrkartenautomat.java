﻿
import java.util.Scanner;

class Fahrkartenautomat
{
	static float zuZahlenderBetrag;
	static int ticketTyp;
	static float eingezahlterGesamtbetrag;
	static float eingeworfeneMünze;
	static float rückgabebetrag;
    static int anzahl;
    static int i;
    static Scanner tastatur = new Scanner(System.in);
    
	static float fahrkartenbestellungErfassen() {
		//Eingabe der Tickets
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		String[] fahrkartenNamen = {
				"Einzelfahrschein Berlin AB",
				"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke",
			 	"Tageskarte Berlin AB",
			 	"Tageskarte Berlin BC",
			 	"Tageskarte Berlin ABC",
			 	"Kleingruppen-Tageskarte Berlin AB",
			 	"Kleingruppen-Tageskarte Berlin BC",
			 	"Kleingruppen-Tageskarte Berlin ABC"
		};
		float[] fahrkartenKosten = {2.9f,3.3f,3.6f,1.9f,8.6f,9f,9.6f,23.5f,24.3f,24.9f};
		
		for (int i=0; i<fahrkartenNamen.length; i++) {
			System.out.println("  "+fahrkartenNamen[i]+" ["+fahrkartenKosten[i]+"] "+"("+(i+1)+")");
		}
		  
		 do {
		   System.out.print("\nIhre Wahl: ");
		   ticketTyp = tastatur.nextInt()-1;
		   
		   if (ticketTyp>=(fahrkartenNamen.length)||ticketTyp<0)
			   System.out.print(" >>falsche Eingabe<<\n");
		   else
			   break;
		}while(true);
		  		   
		   zuZahlenderBetrag=fahrkartenKosten[ticketTyp];
		   
		   do {
		       System.out.print("Anzahl der Tickets: ");
		       anzahl = tastatur.nextInt();
		   } while (anzahl>10 || anzahl<1);
		   
		   zuZahlenderBetrag=zuZahlenderBetrag*anzahl;
		   return zuZahlenderBetrag;
	}
	
	public static float fahrkartenBezahlen(float zuZahlenderBetrag) {
		// Geldeinwurf
		eingezahlterGesamtbetrag = (float) 0.0;
	    while(eingezahlterGesamtbetrag <zuZahlenderBetrag)
	    {
	    	System.out.printf("%s %.2f \n","Noch zu zahlen:", (zuZahlenderBetrag-eingezahlterGesamtbetrag));
	    	System.out.print("Eingabe (min. 0,05 Euro, höchstens 2 Euro): ");
	    	eingeworfeneMünze = tastatur.nextFloat();
	        eingezahlterGesamtbetrag += eingeworfeneMünze;
	    }
	    rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	    return rückgabebetrag;
	}
	
	static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben() {
		//rückgabebetrag=(Math.round(rückgabebetrag*100));
		//rückgabebetrag=rückgabebetrag/100;
		rückgabebetrag = rundung(rückgabebetrag, 4);
		if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n",rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
		          rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
		          rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
		          rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	 	         rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
		          rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	 	         rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n");
	}
	
	private static float rundung (float wert, int nachkomma)
    {
    	float retVal = 0.0f;
    	float Komma = 1.0f;
    	
    	if (nachkomma > 0)
    	{
    		for(int i = 0; i < nachkomma; i++)
    		{
    			Komma = Komma * 10;
    		}
    	}
    	retVal = (Math.round(wert * Komma) / Komma);
    	return retVal;
    }
	
    public static void main(String[] args)
    {
    do {
		//Eingabe der Tickets
    	zuZahlenderBetrag=fahrkartenbestellungErfassen();
        
		// Geldeinwurf
    	rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
    	
    	// Fahrscheinausgabe
    	fahrkartenAusgeben();  
    	
    	// Wechselgeld ausgabe
    	rueckgeldAusgeben();
    }while(true);
    	
    }       
}