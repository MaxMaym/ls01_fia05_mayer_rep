package konsolenausgabe;

public class Aufgabe2 {
	public static void main(String[] args) {
		System.out.print("0!  =                   =");System.out.println("   1");
		System.out.print("1!  = 1                 =");System.out.println("   1");
		System.out.print("2!  = 1 * 2             =");System.out.println("   2");
		System.out.print("3!  = 1 * 2 * 3         =");System.out.println("   6");
		System.out.print("4!  = 1 * 2 * 3 * 4     =");System.out.println("  24");
		System.out.print("5!  = 1 * 2 * 3 * 4 * 5 =");System.out.println(" 120");
	}
}
